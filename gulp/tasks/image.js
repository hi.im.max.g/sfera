import { src, dest }    from 'gulp';
import { path }         from '../path';
import env              from 'gulp-environment';
import imagemin         from 'gulp-imagemin';
import bs               from 'browser-sync';

// Таск для изображений
export const Image = (done) => {
    src(path.image.src)
        .pipe(imagemin([
            imagemin.gifsicle({interlaced: true}),
            imagemin.jpegtran({progressive: true}),
            imagemin.optipng({optimizationLevel: 5}),
            imagemin.svgo({
                plugins: [
                    {removeViewBox: true},
                    {cleanupIDs: false}
                ]
            })
        ]))
        .pipe(dest(path.image.dist))
        .pipe(env.if.development(bs.reload({stream: true})))
        done();
};
